﻿using System.Windows;

namespace Layers
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //IOS os = new Windows();
            //os.Hardware = new HardwareChainik();
            IOS os = new Linux();
            os.Hardware = new HardwarePC();
            _tOut.Text = os.Read();
        }
    }

    #region OS
    public interface IOS
    {
        IHardware Hardware { get; set; }
        string Read();
    }
    public class Windows : IOS
    {
        public IHardware Hardware { get; set; }
        public string Read() => Hardware.Read();
    }
    public class Linux : IOS
    {
        public IHardware Hardware { get; set; }
        public string Read() => Hardware.Read() + 9;
    }
    #endregion

    #region Hardware
    public interface IHardware
    {
        string Read();
    }
    public class HardwarePC : IHardware
    {
        public string Read() => "123";
    }
    public class HardwareChainik : IHardware
    {
        public string Read() => "1";
    }
    #endregion
}
