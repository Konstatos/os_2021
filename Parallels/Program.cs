﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Parallels
{
    class Program
    {
        const int Size = 3000;
        static int[,] Maxtrix1 = new int[Size, Size];
        static int[,] Maxtrix2 = new int[Size, Size];
        static int[,] Maxtrix3 = new int[Size, Size];
        static Random random = new Random();
        private static List<int> result;

        static void DoMatrix(int[,] maxtrix, Action<int> action)
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                    action(maxtrix[i, j]);
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            for (int i = 0; i < Size; i++)
                for (int j = 0; j < Size; j++)
                {
                    Maxtrix1[i, j] = random.Next(0, 10);
                    Maxtrix2[i, j] = random.Next(0, 10);
                }

            //DoMatrix(Maxtrix1, Show);
            Console.WriteLine();
            //DoMatrix(Maxtrix2, Show);
            Console.WriteLine();
            //DoMatrix(Maxtrix1, x => Console.WriteLine(x));

            //Console.ReadKey();

            var sw = Stopwatch.StartNew();
            //SumOneThread();
            //SumManyThreads();
            //SumManyTasks();
            //SumManyLinqFor();
            SumOneLinq();
            //SumManyLinq();
            sw.Stop();

            //DoMatrix(Maxtrix3, Show);
            Console.WriteLine("Ms=" + sw.ElapsedMilliseconds);
            Console.ReadKey();
        }

        private static void SumManyLinq()
        {
            result = Maxtrix1.AsParallel().Cast<int>()
                .OrderBy(x => x).ToList();
        }
        private static void SumOneLinq()
        {
            result = Maxtrix1.Cast<int>()
                .OrderBy(x => x).ToList();
        }

        private static void SumManyLinqFor()
        {
            Parallel.For(0, Size, i =>
            {
                for (int j = 0; j < Size; j++)
                    Calc(i, j);
            });
        }

        private static void SumOneThread()
        {
            for (int i = 0; i < Size; i++)
                for (int j = 0; j < Size; j++)
                {
                    Calc(i, j);
                }
        }

        private static void Calc(int i, int j) =>
            Maxtrix3[i, j] = (int)Math.Pow(Maxtrix1[i, j], Maxtrix2[i, j]);

        private static void SumManyThreads()
        {
            var procCount = 4; // Environment.ProcessorCount;
            var threads = new List<Thread>();
            var delta = Size / procCount + 1;
            var t = 0;
            for (int i = 0; i < Size; i += delta, ++t)
            {
                var from = i;
                var to = from + delta;
                if (to > Size) to = Size;

                var item = new Thread(() =>
                {
                    for (int j = from; j < to; j++)
                        for (int k = 0; k < Size; k++)
                            Calc(j, k);
                });
                threads.Add(item);
                item.Start();
            }
            threads.WaitAll();
        }

        private static void SumManyTasks()
        {
            var procCount = Environment.ProcessorCount;
            var threads = new List<Task>();
            var delta = Size / procCount + 1;
            var t = 0;
            for (int i = 0; i < Size; i += delta, ++t)
            {
                var from = i;
                var to = from + delta;
                if (to > Size) to = Size;
                threads.Add(Task.Run(() =>
                {
                    for (int j = from; j < to; j++)
                        for (int k = 0; k < Size; k++)
                            Calc(j, k);
                }));
            }
            Task.WaitAll(threads.ToArray());
        }

        static void Show(int x)
        {
            Console.Write(x + ", ");
        }
    }

    public static class ThreadExtension
    {
        public static void WaitAll(this IEnumerable<Thread> threads)
        {
            if (threads != null)
            {
                foreach (Thread thread in threads)
                    thread.Join();
            }
        }
    }

}
